package example.model;


import javax.persistence.*;

@Entity
@Table(name = "meta")
public class Meta {

    private long id;
    private String description;
    private int tableRow;
    private int tableColumn;

    public Meta() {
    }

    public Meta(long id, String description, int tableRow, int tableColumn) {
        this.id = id;
        this.description = description;
        this.tableRow = tableRow;
        this.tableColumn = tableColumn;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "description", nullable = false)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "table_row", nullable = false)
    public int getTableRow() {
        return tableRow;
    }

    public void setTableRow(int tableRow) {
        this.tableRow = tableRow;
    }

    @Column(name = "table_column", nullable = false)
    public int getTableColumn() {
        return tableColumn;
    }

    public void setTableColumn(int tableColumn) {
        this.tableColumn = tableColumn;
    }

    @Override
    public String toString() {
        return "Meta{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", tableRow=" + tableRow +
                ", tableColumn=" + tableColumn +
                '}';
    }
}