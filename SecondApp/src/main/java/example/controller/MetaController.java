package example.controller;

import example.exception.ResourceNotFoundException;
import example.model.Meta;
import example.repository.MetaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/v1")
public class MetaController{
    @Autowired
    private MetaRepository metaRepository;

    @GetMapping("/meta")
    public List<Meta> getAllMeta() {
        return metaRepository.findAll();
    }

    @GetMapping("/meta/{id}")
    public ResponseEntity<Meta> getMetaById(@PathVariable(value = "id") Long metaId)
            throws ResourceNotFoundException {
        Meta meta = metaRepository.findById(metaId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + metaId));
        return ResponseEntity.ok().body(meta);
    }

}