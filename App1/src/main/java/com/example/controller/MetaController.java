package com.example.controller;

import java.util.List;

import javax.validation.Valid;

import com.example.exception.ResourceNotFoundException;
import com.example.model.Meta;
import com.example.repository.MetaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1")
public class MetaController{
    @Autowired
    private MetaRepository metaRepository;

    @PostMapping("/table")
    public Meta createMeta(@Valid @RequestBody Meta meta) {
        return metaRepository.save(meta);
    }

    @PutMapping("/table/{id}")
    public ResponseEntity<Meta> updateMeta(@PathVariable(value = "id") Long metaId,
        @Valid @RequestBody Meta metaDetails) throws ResourceNotFoundException {
        Meta meta = metaRepository.findById(metaId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + metaId));

        meta.setDescription(metaDetails.getDescription());
        meta.setTableRow(metaDetails.getTableRow());
        meta.setTableColumn(metaDetails.getTableColumn());

        final Meta updatedMeta = metaRepository.save(meta);
        return ResponseEntity.ok(updatedMeta);
    }
}