<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<html>
<head>
    <title>Spring MVC Form Handling</title>
</head>
<body>

<h2>Table Information</h2>
<form:form method = "GET" action = "/addTable">
    <table>
        <tr>
            <td><form:label path = "row">Row</form:label></td>
            <td><form:input path = "row" /></td>
        </tr>
        <tr>
            <td><form:label path = "column">Column</form:label></td>
            <td><form:input path = "column" /></td>
        </tr>
        <tr>
            <td><form:label path = "id">id</form:label></td>
            <td><form:input path = "id" /></td>
        </tr>
        <tr>
            <td colspan = "2">
                <input type = "submit" value = "Submit"/>
            </td>
        </tr>
    </table>
</form:form>
</body>
</html>
