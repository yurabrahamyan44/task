package com.example;

import com.example.model.Meta;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MetaControllerIntegration {
    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    private String getRootUrl() {
        return "http://localhost:" + port;
    }

    @Test
    public void contextLoads() {

    }
    @Test
    public void testGetAllMetas() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/employees",
                HttpMethod.GET, entity, String.class);

        assertNotNull(response.getBody());
    }

    @Test
    public void testGetEmployeeById() {
        Meta meta = restTemplate.getForObject(getRootUrl() + "/tables/1", Meta.class);
        System.out.println(meta.getDescription());
        assertNotNull(meta);
    }

    @Test
    public void testCreateEmployee() {
        Meta meta = new Meta();

        meta.setDescription("SomeDescription");
        meta.setTableRow(5);
        meta.setTableColumn(5);

        ResponseEntity<Meta> postResponse = restTemplate.postForEntity(getRootUrl() + "/tables", meta, Meta.class);
        assertNotNull(postResponse);
        assertNotNull(postResponse.getBody());
    }

    @Test
    public void testUpdateMeta() {
        int id = 1;
        Meta meta = restTemplate.getForObject(getRootUrl() + "/tables/" + id, Meta.class);
        meta.setTableRow(7);
        meta.setTableColumn(5);

        restTemplate.put(getRootUrl() + "/employees/" + id, meta);

        Meta updatedMeta = restTemplate.getForObject(getRootUrl() + "/employees/" + id, Meta.class);
        assertNotNull(updatedMeta);
    }

    @Test
    public void testDeleteEmployee() {
        int id = 2;
        Meta meta = restTemplate.getForObject(getRootUrl() + "/employees/" + id, Meta.class);
        assertNotNull(meta);

        restTemplate.delete(getRootUrl() + "/employees/" + id);

        try {
            meta = restTemplate.getForObject(getRootUrl() + "/employees/" + id, Meta.class);
        } catch (final HttpClientErrorException e) {
            assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }
}
