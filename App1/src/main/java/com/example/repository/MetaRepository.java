package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.model.Meta;

public interface MetaRepository extends JpaRepository<Meta, Long>{
}
